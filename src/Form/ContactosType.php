<?php

namespace App\Form;

use App\Entity\Contactos;
use App\Entity\Telefonos;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ContactosType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', TextType::class, [
                    'attr' => [
                        'onkeyup' => "saveValue(this)",
                    ],
                ])
                ->add('apellido1', TextType::class, [
                    'attr' => [
                        'onkeyup' => "saveValue(this)",
                    ],
                ])
                ->add('apellido2', TextType::class, [
                    'attr' => [
                        'onkeyup' => "saveValue(this)",
                    ],
                ])
                ->add('dni', TextType::class, [
                    'attr' => [
                        'onkeyup' => "saveValue(this)",
                    ],
        ]);
        $builder->add('telefonos', CollectionType::class, [
            'entry_type' => TelefonosType::class,
            'entry_options' => ['label' => false],
        ]);
        
        //aquí le meto el formulario hecho de la entidad de emails
        $builder->add('emails', EmailsType::class);
    }

    public function configureOptions(OptionsResolver $resolver) {

        $resolver->setDefaults([
            'data_class' => Contactos::class,
        ]);
    }

}
