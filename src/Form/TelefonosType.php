<?php
namespace App\Form;

use App\Entity\Telefonos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TelefonosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('telefono', TextType::class,[
            'attr'=>[
                'onkeyup'=>"saveValue(this)",
            ],
        ]);
       
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Telefonos::class,
        ]);
    }
}
