<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contactos
 *
 * @ORM\Table(name="contactos")
 * @ORM\Entity(repositoryClass="App\Repository\ContactosRepository")
 */
class Contactos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido1", type="string", length=255, nullable=false)
     */
    private $apellido1;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido2", type="string", length=255, nullable=false)
     */
    private $apellido2;

    /**
     * @var int
     *
     * @ORM\Column(name="dni", type="integer", nullable=false)
     */
    private $dni;
    
    /**
     * @ORM\OneToMany(targetEntity="Telefonos", mappedBy="idContacto", cascade={"persist", "remove"})
     */
    private $telefonos;
    
    /**
     * @ORM\OneToOne(targetEntity="Emails", mappedBy="idContacto", cascade={"persist", "remove"})
     */
    private $emails;
    
    /**
     *@ORM\OneToMany(targetEntity="Direcciones", mappedBy="idContacto", cascade={"persist", "remove"})
     */
    private $direcciones;

    public function __construct()
    {
        $this->telefonos = new ArrayCollection();
        $this->direcciones = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido1()
    {
        return $this->apellido1;
    }

    public function setApellido1(string $apellido1): self
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    public function getApellido2()
    {
        return $this->apellido2;
    }

    public function setApellido2(string $apellido2): self
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    public function getDni()
    {
        return $this->dni;
    }

    public function setDni(int $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * @return Collection|Telefonos[]
     */
    public function getTelefonos(): Collection
    {
        return $this->telefonos;
    }

    public function addTelefono(Telefonos $telefono): self
    {
        if (!$this->telefonos->contains($telefono)) {
            $this->telefonos[] = $telefono;
            $telefono->setIdContacto($this);
        }

        return $this;
    }

    public function removeTelefono(Telefonos $telefono): self
    {
        if ($this->telefonos->contains($telefono)) {
            $this->telefonos->removeElement($telefono);
            // set the owning side to null (unless already changed)
            if ($telefono->getIdContacto() === $this) {
                $telefono->setIdContacto(null);
            }
        }

        return $this;
    }

    public function getEmails()
    {
        return $this->emails;
    }

    public function setEmails(Emails $emails): self
    {
        $this->emails = $emails;

        // set (or unset) the owning side of the relation if necessary
        $newIdContacto = $emails === null ? null : $this;
        if ($newIdContacto !== $emails->getIdContacto()) {
            $emails->setIdContacto($newIdContacto);
        }

        return $this;
    }

    /**
     * @return Collection|Direcciones[]
     */
    public function getDirecciones(): Collection
    {
        return $this->direcciones;
    }

    public function addDireccione(Direcciones $direccione): self
    {
        if (!$this->direcciones->contains($direccione)) {
            $this->direcciones[] = $direccione;
            $direccione->setIdContacto($this);
        }

        return $this;
    }

    public function removeDireccione(Direcciones $direccione): self
    {
        if ($this->direcciones->contains($direccione)) {
            $this->direcciones->removeElement($direccione);
            // set the owning side to null (unless already changed)
            if ($direccione->getIdContacto() === $this) {
                $direccione->setIdContacto(null);
            }
        }

        return $this;
    }
    
 


}
