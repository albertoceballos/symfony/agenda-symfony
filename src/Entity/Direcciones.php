<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Direcciones
 *
 * @ORM\Table(name="direcciones", indexes={@ORM\Index(name="id_contacto", columns={"id_contacto"})})
 * @ORM\Entity
 */
class Direcciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="calle", type="string", length=255, nullable=false)
     */
    private $calle;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=255, nullable=false)
     */
    private $ciudad;

    /**
     * @var \Contactos
     *
     * @ORM\ManyToOne(targetEntity="Contactos", inversedBy="direcciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contacto", referencedColumnName="id")
     * })
     */
    private $idContacto;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCalle(): string
    {
        return $this->calle;
    }

    public function setCalle(string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    public function getCiudad(): string
    {
        return $this->ciudad;
    }

    public function setCiudad(string $ciudad): self
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function getIdContacto(): Contactos
    {
        return $this->idContacto;
    }

    public function setIdContacto(Contactos $idContacto): self
    {
        $this->idContacto = $idContacto;

        return $this;
    }


}
