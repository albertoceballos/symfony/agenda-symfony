<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telefonos
 *
 * @ORM\Table(name="telefonos", indexes={@ORM\Index(name="telefonos_ibfk_1", columns={"id_contacto"})})
 * @ORM\Entity
 */
class Telefonos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="telefono", type="integer", nullable=true)
     */
    private $telefono;

    /**
     * @var \Contactos
     *
     * @ORM\ManyToOne(targetEntity="Contactos", inversedBy="telefonos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contacto", referencedColumnName="id")
     * })
     */
    private $idContacto;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono($telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getIdContacto(): Contactos
    {
        return $this->idContacto;
    }

    public function setIdContacto(Contactos $idContacto): self
    {
        $this->idContacto = $idContacto;

        return $this;
    }


}
