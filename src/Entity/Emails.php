<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Emails
 *
 * @ORM\Table(name="emails", indexes={@ORM\Index(name="id_contacto", columns={"id_contacto"})})
 * @ORM\Entity
 */
class Emails
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var \Contactos
     *
     * @ORM\OneToOne(targetEntity="Contactos", inversedBy="emails")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contacto", referencedColumnName="id")
     * })
     */
    private $idContacto;

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIdContacto(): Contactos
    {
        return $this->idContacto;
    }

    public function setIdContacto(Contactos $idContacto): self
    {
        $this->idContacto = $idContacto;

        return $this;
    }

   


}
