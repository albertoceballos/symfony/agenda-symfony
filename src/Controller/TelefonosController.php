<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Telefonos;
use App\Form\TelefonosType;

use App\Form\EmailsType;

class TelefonosController extends AbstractController
{
    /**
     * @Route("/telefonos", name="telefonos")
     */
    public function index()
    {
        $form_telefono= $this->createForm(TelefonosType::class);
        $form_email= $this->createForm(EmailsType::class);
        return $this->render('telefonos/index.html.twig', [
            'form_telefono'=>$form_telefono->createView(),
            'form_email'=>$form_email->createView(),
        ]);
    }
}
