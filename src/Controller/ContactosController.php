<?php

namespace App\Controller;

use App\Entity\Contactos;
use App\Entity\Telefonos;
use App\Entity\Emails;
use App\Form\ContactosType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * @Route("/contactos")
 */
class ContactosController extends AbstractController {

    /**
     * @Route("/", name="contactos_index", methods={"GET","POST"})
     */
    public function index(Request $request) {
        $contactos = $this->getDoctrine()
                ->getRepository(Contactos::class)
                ->findBy([], ['id' => 'ASC']);


        $form_ordena = $this->createFormBuilder()
                ->add('ordena', ChoiceType::class, [
                    'label' => 'Ordenar por:',
                    'choices' => [
                        'Id' => 'id',
                        'Nombre' => 'nombre',
                        '1er Apellido' => 'apellido1',
                        '2º Apellido' => 'apellido2',
                    ]
                ])
                ->add('direccion', ChoiceType::class,[
                    'label'=>false,
                    'choices'=>[
                      'Ascendente'=>'ASC',
                      'Descendente'=>'DESC',  
                    ],
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'Ordenar',
                ])
                ->getForm();
        $form_ordena->handleRequest($request);
        if($form_ordena->isSubmitted()){
            
            $campos=$request->request->get('form');
            $orden=$campos['ordena'];
            $direccion=$campos['direccion'];
            
            
            $contactos= $this->getDoctrine()->getRepository(Contactos::class)
                    ->findBy([],[$orden=>$direccion]);
        }
        

        return $this->render('contactos/index.html.twig', [
                    'contactos' => $contactos,
                    'form_ordena'=>$form_ordena->createView(),
        ]);
    }

    /**
     * @Route("/new/{add}", name="contactos_new", defaults={"add":0}, methods={"GET","POST"})
     */
    public function nuevo(Request $request, $add) {

        $entityManager = $this->getDoctrine()->getManager();

        $contacto = new Contactos();

        /* Para poder agregar los teléfonos al formulario de colecciones hay que pasarle a la colección
          de telefonos dentro del objeto Contactos, al menos un objeto telefono vacío,creo tantos como
          me pasa el parametro add que se va incrementando al añadir más telefonos: */

        $tel = [];
        for ($i = 0; $i < $add + 1; $i++) {
            $tel[$i] = new Telefonos();
            $contacto->getTelefonos()->add($tel[$i]);
            //IMPORTANTE!!!:Le paso al objeto teléfono el IdContacto que tiene que ser un objeto de tipo Contacto:
            $tel[$i]->setIdContacto($contacto);
        }

        $form = $this->createForm(ContactosType::class, $contacto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {



            $entityManager->persist($contacto);

            /* solo persisto el contacto porque la coleccion de telefonos se persisten en cascada
             * automaticamente al tener en la entidad la anotación cascade={"persist"}
             */
            var_dump($contacto);

            $entityManager->flush();


            return $this->redirectToRoute('contactos_index');
        }


        return $this->render('contactos/new.html.twig', [
                    'contacto' => $contacto,
                    'form' => $form->createView(),
                    'add' => $add,
        ]);
    }

    /**
     * @Route("/{id}", name="contactos_show", methods={"GET"})
     */
    public function show(Contactos $contacto) {
        return $this->render('contactos/show.html.twig', [
                    'contacto' => $contacto,
        ]);
    }

    /**
     * @Route("/{id}/edit/{add}", defaults={"add":0}, name="contactos_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contactos $contacto, $add) {


        ///Para añadir nuevos telefónos al pulsar añadir:
        if ($add > 0) {
            for ($i = 0; $i < $add + 1; $i++) {
                $tel[$i] = new Telefonos();
                $contacto->getTelefonos()->add($tel[$i]);
                $tel[$i]->setIdContacto($contacto);
            }
        }

        //Si no hay ningún teléfono todavía, añade un campo de email vacío:
        if ($contacto->getTelefonos()->isEmpty()) {
            $tel = new Telefonos();
            $contacto->getTelefonos()->add($tel);
            $tel->setIdContacto($contacto);
        }

        $form = $this->createForm(ContactosType::class, $contacto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contactos_index', [
                        'id' => $contacto->getId(),
            ]);
        }
        $edit = true;
        return $this->render('contactos/edit.html.twig', [
                    'contacto' => $contacto,
                    'form' => $form->createView(),
                    'edit' => $edit,
                    'add' => $add,
        ]);
    }

    /**
     * @Route("/{id}", name="contactos_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contactos $contacto) {
        if ($this->isCsrfTokenValid('delete' . $contacto->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contacto);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contactos_index');
    }
    
    /**
     * @Route("/busqueda", name="busqueda_ajax", methods={"POST"})
     */
    public function busquedaAjax(Request $request){
        if($request->isXmlHttpRequest()){
            $busqueda=$request->request->get('busqueda');
            $contactos_repo= $this->getDoctrine()->getRepository(Contactos::class);
            $contactos=$contactos_repo->buscarContactoLike($busqueda);
            
            return $this->render('contactos/tabla.html.twig',[
                'contactos'=>$contactos,
            ]);
            
        }
      
    }

}
